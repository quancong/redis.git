package com.redisbloomfilter.controller;

import com.redisbloomfilter.service.CacheHitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CacheHitController {

    @Autowired
    private CacheHitService cacheHitService;

    @RequestMapping("/cache/hit")
    public String cacheHit(Integer key){
        return cacheHitService.getKey(key);
    }
}
